const form = document.getElementById('form');
const yearInput = document.getElementById('year');
const monthInput = document.getElementById('month');
const hoursInput = document.getElementById('hours');
const countryInput = document.getElementById('country');

const resultMain = document.getElementById('result-main');
const resultCalculation = document.getElementById('result-calculation');
const resultHolidays = document.getElementById('result-holidays');

const geolocationError = document.getElementById('geolocation-error');

const geolocation = navigator.geolocation;

let countries;

form.addEventListener('submit', handleSubmit);
document.addEventListener('DOMContentLoaded', () => {
    const today = new Date();
    yearInput.value = today.getFullYear();
    hoursInput.value = 38.5;

    const range = Array(12).fill(0);
    const options = range.map((_, monthIndex) => {
        const option = document.createElement('option');
        option.value = monthIndex + 1;
        option.innerText = GetMonthName(monthIndex + 1);
        return option;
    });
    monthInput.clearChildren();
    monthInput.append(...options);
    monthInput.value = today.getMonth() + 1;

    FetchCountries().then(_ => {
        const options = countries.map(country => {
            const option = document.createElement('option');
            option.value = country.Name;
            option.innerText = country.Name;

            if (country.Name === 'Austria') option.selected = true;

            return option;
        });
        countryInput.clearChildren();
        countryInput.append(...options);
    });
});

function HandleUseLocation() {
    GetCurrentPosition().then(position => {
        FetchCountryForPosition(position).then(result => {
            const country = result.results[0].components.country;
            countryInput.value = country;
            console.log(`Set country to ${country}`);
        });
    });
}

async function GetCurrentPosition() {
    return new Promise((resolve, reject) => {
        geolocation.getCurrentPosition(position => {
            resolve(position);
        }, error => {
            geolocationError.showModal();
            reject(error);
        });
    });
}

function handleSubmit(e) {
    e.preventDefault();
    calculate();
}

function calculate() {
    const year = yearInput.value;
    const month = monthInput.value;
    const hours = hoursInput.value;
    const dayHours = hours / 5;


    FetcHolidays(year, month, countryInput.value).then(result => {
        const range = Array(result.lastDay.getDate()).fill(0);
        const workingDays = range
            .map((_, i) => new Date(year, month - 1, i + 1))
            .filter(day => !HolidaysContain(result.holidays, day))
            .map(day => day.getDay())
            .filter(day => day !== 0 && day !== 6);

        const monthHours = dayHours * workingDays.length;

        resultMain.innerHTML = `You are <b>expected</b> to do <b>${monthHours} hours</b> in ${GetMonthName(month)} ${year}.`;
        resultCalculation.innerHTML = `Calculated with  <b>${workingDays.length} working days</b> and <b>${dayHours} hours</b> per day. ${workingDays.length + result.holidays.length} weekdays.`
        resultHolidays.innerHTML = `Got ${result.holidays.length} holiday${result.holidays.length !== 1 ? 's' : ''}: ${result.holidays.map(holiday => GetHolidayName(holiday)).join(', ')}`;
        resultMain.removeAttribute('hidden');
        resultCalculation.removeAttribute('hidden');
        resultHolidays.removeAttribute('hidden');
    });
}

function HolidaysContain(holidays, day) {
    return holidays.some(holiday => {
        const comparee = new Date(day);
        comparee.setUTCHours(0);
        const date = new Date(holiday.startDate);
        date.setUTCHours(0);
        return date.getTime() === comparee.getTime();
    });
}

function GetMonthName(month) {
    const date = new Date(2020, month - 1, 1);
    return date.toLocaleString('en-us', {month: 'long'});
}

function GetHolidayName(holiday) {
    console.log('Holiday', holiday);
    const userLang = navigator.language.split('-')[0];
    console.log('User language', userLang, navigator.language);

    const localeHoliday = holiday.name
        .filter((name) => name.language.toLocaleLowerCase() === userLang.toLowerCase())[0];
    console.log('Locale holiday', localeHoliday);
    return localeHoliday.text;
}

// API: https://openholidaysapi.org/PublicHolidays?countryIsoCode=AT&languageIsoCode=DE&validFrom=2023-01-01&validTo=2023-12-31&subdivisionCode=DE-AT
async function FetcHolidays(year, month, country) {
    const lastDayOfMonth = LastDayOfMonth(year, month);
    const response = await fetch(`https://openholidaysapi.org/PublicHolidays`
        + `?countryIsoCode=${GetCountryByName(country).Code}`
        + `&validFrom=${year}-${month}-01`
        + `&validTo=${year}-${month}-${lastDayOfMonth.getDate()}`);
    const result = await response.json();

    return {
        holidays: result,
        lastDay: lastDayOfMonth
    };
}

// API: https://pkgstore.datahub.io/core/country-list/data_json/data/8c458f2d15d9f2119654b29ede6e45b8/data_json.json
async function FetchCountries() {
    const response = await fetch('https://pkgstore.datahub.io/core/country-list/data_json/data/8c458f2d15d9f2119654b29ede6e45b8/data_json.json');
    countries = await response.json();
}

// API: https://api.opencagedata.com/geocode/v1/json
// ?q=48.2081743+16.3738189
// &key=b104af3fa81b4f238939b87dade236c8
// API key var: OPENCAGE_API_KEY
async function FetchCountryForPosition(position) {
    const response = await fetch(`https://api.opencagedata.com/geocode/v1/json?key=$OPENCAGE_API_KEY&language=en&q=${position.coords.latitude}+${position.coords.longitude}`);
    return await response.json();
}

function GetCountryByName(name) {
    return countries.find(country => country.Name === name);
}

function LastDayOfMonth(Year, Month) {
    return new Date((new Date(Year, Month, 1)) - 1)
}

if (typeof Element.prototype.clearChildren === 'undefined') {
    Object.defineProperty(Element.prototype, 'clearChildren', {
        configurable: true,
        enumerable: false,
        value: function () {
            while (this.firstChild) this.removeChild(this.lastChild);
        }
    });
}